pub trait View: std::fmt::Debug {
    fn fetch(&self) -> &wgpu::TextureView;
}

#[derive(Debug)]
pub struct Screen(pub(crate) wgpu::SwapChainFrame);

impl View for Screen {
    fn fetch(&self) -> &wgpu::TextureView {
        &self.0.output.view
    }
}

#[derive(Debug)]
pub struct Texture(pub(crate) wgpu::TextureView);

impl View for Texture {
    fn fetch(&self) -> &wgpu::TextureView {
        &self.0
    }
}
