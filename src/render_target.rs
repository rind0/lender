pub trait RenderTarget: std::fmt::Debug + 'static {
    type View: crate::View;
    fn resize(&mut self, device: &wgpu::Device, width: u32, height: u32);
    fn format(&self) -> wgpu::TextureFormat;
    fn width(&self) -> u32;
    fn height(&self) -> u32;
    fn get_next_texture(&mut self) -> Result<Self::View, wgpu::SwapChainError>;
    fn submit<I: IntoIterator<Item = wgpu::CommandBuffer>>(
        &self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        command_buffers: I,
    );
}

#[derive(Debug)]
pub struct Screen {
    window_surface: wgpu::Surface,
    swap_chain_descriptor: wgpu::SwapChainDescriptor,
    swap_chain: wgpu::SwapChain,
}

impl Screen {
    pub fn new(surface: wgpu::Surface, size: (u32, u32), device: &wgpu::Device) -> Self {
        let swap_chain_descriptor = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
            format: wgpu::TextureFormat::Bgra8Unorm,
            width: size.0,
            height: size.1,
            present_mode: wgpu::PresentMode::Mailbox,
        };
        let swap_chain = device.create_swap_chain(&surface, &swap_chain_descriptor);
        Self {
            window_surface: surface,
            swap_chain_descriptor,
            swap_chain,
        }
    }
}

impl RenderTarget for Screen {
    type View = crate::view::Screen;

    fn resize(&mut self, device: &wgpu::Device, width: u32, height: u32) {
        self.swap_chain_descriptor.width = width;
        self.swap_chain_descriptor.height = height;
        self.swap_chain =
            device.create_swap_chain(&self.window_surface, &self.swap_chain_descriptor);
    }

    fn format(&self) -> wgpu::TextureFormat {
        self.swap_chain_descriptor.format
    }

    fn width(&self) -> u32 {
        self.swap_chain_descriptor.width
    }

    fn height(&self) -> u32 {
        self.swap_chain_descriptor.height
    }

    fn get_next_texture(&mut self) -> Result<Self::View, wgpu::SwapChainError> {
        self.swap_chain.get_current_frame().map(crate::view::Screen)
    }

    fn submit<I: IntoIterator<Item = wgpu::CommandBuffer>>(
        &self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        command_buffers: I,
    ) {
        queue.submit(command_buffers);
    }
}

#[derive(Debug)]
pub struct Texture {
    size: wgpu::Extent3d,
    texture: wgpu::Texture,
    format: wgpu::TextureFormat,
    buffer: wgpu::Buffer,
}

impl Texture {
    pub fn new(device: &wgpu::Device, size: (u32, u32)) -> Self {
        let size = wgpu::Extent3d {
            width: size.0,
            height: size.1,
            depth_or_array_layers: 1,
        }
        let texture_label = create_debug_label!("");
        Self {
            size,
            texture,
            format,
            buffer,
        }
    }
}
