use anyhow::Context;
pub use render_target::RenderTarget;
pub use view::View;

pub mod render_target;
pub mod texture;
pub mod view;

macro_rules! create_debug_label {
    ($($arg:tt)*) => {
        if cfg!(feature = "render_debug_labels") {
            Some($($arg)*)
        } else {
            None
        }
    };
}

#[derive(Debug)]
pub struct Descriptor {
    device: wgpu::Device,
    queue: wgpu::Queue,
    msaa_sample_count: u32,
}

impl Descriptor {
    pub fn new(instance: wgpu::Instance, surface: Option<&wgpu::Surface>) -> anyhow::Result<Self> {
        use futures::executor::block_on;
        let msaa_sample_count = 4;
        let adapter = block_on(instance.request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::default(),
            compatible_surface: surface,
        }))
        .context("Failed to request adapter.")?;
        let device_label = create_debug_label!("");
        let (device, queue) = block_on(adapter.request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: wgpu::Features::PUSH_CONSTANTS,
                limits: wgpu::Limits::default(),
            },
            None,
        ))
        .context("Failed to request device.")?;
        Ok(Self {
            device,
            queue,
            msaa_sample_count,
        })
    }
}

struct Frame<'a, T: RenderTarget> {
    view_data: Box<(wgpu::CommandEncoder, T::View)>,
    render_pass: wgpu::RenderPass<'a>,
}

impl<'a, T: RenderTarget> Frame<'static, T> {
    fn get(&mut self) -> &mut Frame<'a, T> {
        unsafe { std::mem::transmute::<_, &mut Frame<'a, T>>(self) }
    }
}

pub trait RenderBackend: downcast_rs::Downcast {
    fn set_viewport(&mut self, width: u32, height: u32);
    fn begin_frame(&mut self, clear: wgpu::Color);
    fn end_frame(&mut self);
}
downcast_rs::impl_downcast!(RenderBackend);

pub struct WgpuRenderBackend<T: RenderTarget> {
    descriptor: Descriptor,
    target: T,
    frame_buffer_view: wgpu::TextureView,
    depth_texture_view: wgpu::TextureView,
    current_frame: Option<Frame<'static, T>>,
}

impl<T: RenderTarget> WgpuRenderBackend<T> {
    pub fn new(descriptor: Descriptor, target: T) -> anyhow::Result<Self> {
        let extent = wgpu::Extent3d {
            width: target.width(),
            height: target.height(),
            depth_or_array_layers: 1,
        };

        let frame_buffer_label = create_debug_label!("Frame buffer texture");
        let frame_buffer = descriptor.device.create_texture(&wgpu::TextureDescriptor {
            label: frame_buffer_label,
            size: extent,
            mip_level_count: 1,
            sample_count: descriptor.msaa_sample_count,
            dimension: wgpu::TextureDimension::D2,
            format: target.format(),
            usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
        });
        let frame_buffer_view = frame_buffer.create_view(&Default::default());

        let depth_label = create_debug_label!("Depth texture");
        let depth_texture = descriptor.device.create_texture(&wgpu::TextureDescriptor {
            label: depth_label,
            size: extent,
            mip_level_count: 1,
            sample_count: descriptor.msaa_sample_count,
            dimension: wgpu::TextureDimension::D2,
            format: wgpu::TextureFormat::Depth24PlusStencil8,
            usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
        });
        let depth_texture_view = depth_texture.create_view(&Default::default());
        Ok(Self {
            descriptor,
            target,
            frame_buffer_view,
            depth_texture_view,
            current_frame: None,
        })
    }

    pub fn descriptor(self) -> Descriptor {
        self.descriptor
    }

    pub fn target(&self) -> &T {
        &self.target
    }

    pub fn device(&self) -> &wgpu::Device {
        &self.descriptor.device
    }
}

impl WgpuRenderBackend<render_target::Screen> {
    pub fn for_window<W: raw_window_handle::HasRawWindowHandle>(
        window: &W,
        size: (u32, u32),
        trace_path: Option<&std::path::Path>,
    ) -> anyhow::Result<Self> {
        let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        let descriptor = Descriptor::new(instance, Some(&surface))?;
        let target = render_target::Screen::new(surface, size, &descriptor.device);
        Self::new(descriptor, target)
    }
}

impl<T: RenderTarget + 'static> RenderBackend for WgpuRenderBackend<T> {
    fn set_viewport(&mut self, width: u32, height: u32) {
        let width = std::cmp::max(width, 1);
        let height = std::cmp::max(height, 1);
        self.target.resize(&self.descriptor.device, width, height);

        let extent = wgpu::Extent3d {
            width,
            height,
            depth_or_array_layers: 1,
        };
        let frame_buffer_label = create_debug_label!("Frame buffer texture");
        let frame_buffer = self
            .descriptor
            .device
            .create_texture(&wgpu::TextureDescriptor {
                label: frame_buffer_label,
                size: extent,
                mip_level_count: 1,
                sample_count: self.descriptor.msaa_sample_count,
                dimension: wgpu::TextureDimension::D2,
                format: self.target.format(),
                usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
            });
        self.frame_buffer_view = frame_buffer.create_view(&Default::default());
        let depth_label = create_debug_label!("Depth texture");
        let depth_texture = self
            .descriptor
            .device
            .create_texture(&wgpu::TextureDescriptor {
                label: depth_label,
                size: extent,
                mip_level_count: 1,
                sample_count: self.descriptor.msaa_sample_count,
                dimension: wgpu::TextureDimension::D2,
                format: wgpu::TextureFormat::Depth24PlusStencil8,
                usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
            });
        self.depth_texture_view = depth_texture.create_view(&Default::default());
    }

    fn begin_frame(&mut self, clear: wgpu::Color) {
        let frame_output = match self.target.get_next_texture() {
            Ok(frame) => frame,
            Err(e) => {
                println!("Unable to begin frame. {:?}", e);
                self.target.resize(
                    &self.descriptor.device,
                    self.target.width(),
                    self.target.height(),
                );
                return;
            }
        };
        let label = create_debug_label!("Draw encoder");
        let draw_encoder = self
            .descriptor
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor { label });
        let mut view_data = Box::new((draw_encoder, frame_output));
        let (color_view, resolve_target) = if self.descriptor.msaa_sample_count >= 2 {
            (&self.frame_buffer_view, Some(view_data.1.fetch()))
        } else {
            (view_data.1.fetch(), None)
        };
        let render_pass_label = create_debug_label!("Render pass");
        let render_pass = view_data.0.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: render_pass_label,
            color_attachments: &[wgpu::RenderPassColorAttachment {
                view: color_view,
                resolve_target,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color {
                        r: f64::from(clear.r) / 255.0,
                        g: f64::from(clear.g) / 255.0,
                        b: f64::from(clear.b) / 255.0,
                        a: f64::from(clear.a) / 255.0,
                    }),
                    store: true,
                },
            }],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: &self.depth_texture_view,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(0.0),
                    store: true,
                }),
                stencil_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(0),
                    store: true,
                }),
            }),
        });
        self.current_frame = Some(Frame {
            render_pass: unsafe {
                std::mem::transmute::<_, wgpu::RenderPass<'static>>(render_pass)
            },
            view_data,
        });
    }

    fn end_frame(&mut self) {
        if let Some(frame) = self.current_frame.take() {
            drop(frame.render_pass);
            let draw_encoder = frame.view_data.0;
            self.target.submit(
                &self.descriptor.device,
                &self.descriptor.queue,
                vec![draw_encoder.finish()],
            );
        }
    }
}
