pub struct View;

pub fn create_depth(descriptor: &crate::Descriptor, label: &str) {
    let depth_texture = descriptor.device.create_texture(&wgpu::TextureDescriptor {
        label: label,
        size: extent,
        mip_level_count: 1,
        sample_count: descriptor.msaa_sample_count,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Depth24PlusStencil8,
        usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
    });
    let depth_texture_view = depth_texture.create_view(&Default::default());
}
